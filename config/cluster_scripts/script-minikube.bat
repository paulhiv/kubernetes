
REM minikube delete
REM minikube delete
REM minikube start --disk-size 20g --memory 8g --cpus 4

REM Azure Install
    REM az login
    REM az aks get-credentials --resource-group kubernetes --name kubernetes
    REM kubectl get nodes

REM HTTP routing
    REM az aks enable-addons --resource-group kubernetes --name kubernetes --addons http_application_routing
    REM az aks show --resource-group kubernetes --name kubernetes --query addonProfiles.httpApplicationRouting.config.HTTPApplicationRoutingZoneName -o table

REM depot helm
helm repo add appscode https://charts.appscode.com/stable/
helm repo add stable https://kubernetes-charts.storage.googleapis.com/
helm repo update

REM helm install
helm install kubedb-operator appscode/kubedb --version v0.12.0 -n kube-system 
helm install kubedb-catalog appscode/kubedb-catalog --version v0.12.0 -n kube-system
helm install ingress-operator -f ./config/helm-ingress-nginx.yml stable/nginx-ingress --version 1.36.0 -n kube-system

REM configmaps
REM kubectl create configmap mysql-init-script --from-file=./config/kubedb/mysql-init.sql

REM Setup All
kubectl apply -f ./applyall.yml

REM Setup step by step
    REM mysql
    kubectl apply -f ./config/kubedb/secret.yml -n kubedb
    kubectl apply -f ./config/kubedb/mysql.yml -n kubedb

    REM phpmyadmin
    kubectl apply -f ./config/kubedb/phpmyadmin/deployment.yml -n kubedb
    kubectl apply -f ./config/kubedb/phpmyadmin/service.yml -n kubedb
    kubectl apply -f ./config/kubedb/phpmyadmin/ingress.yml -n kubedb

    REM wordpress
    kubectl apply -f ./config/wordpress/secret.yml -n wordpress
    kubectl apply -f ./config/wordpress/deployment.yml -n wordpress
    kubectl apply -f ./config/wordpress/service.yml -n wordpress
    kubectl apply -f ./config/wordpress/ingress.yml -n wordpress

    REM rbac
    kubectl apply -f ./data/rbac/prometheus.yml

REM Delete Kind


REM finish !
echo Installation finish !


