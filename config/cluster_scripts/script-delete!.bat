    REM mysql
    kubectl delete secret secret-mysql -n kubedb
    kubectl delete MySQL kubedb-mysql -n kubedb

    REM phpmyadmin
    kubectl delete deployment deployment-phpmyadmin -n kubedb
    kubectl delete service service-phpmyadmin -n kubedb
    kubectl delete ingress ingress-phpmyadmin -n kubedb

    REM wordpress
    kubectl delete secret secret-wordpress -n wordpress
    kubectl delete deployment deployment-wordpress -n wordpress
    kubectl delete service service-wordpress -n wordpress
    kubectl delete ingress ingress-wordpress -n wordpress

    REM rbac